/* create user form validatrion */

$("#addUser1").validate({
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: "/check-email-address",
                type: "post",
                data: {
                    email: function () {
                        return $("#email").val();
                    }
                }
            }
        },
        password: {
            required: true,
            minlength: 5,
        },
        mobileNumber: {
            required: true,
            minlength: 10
        },
        image: {
            /* // extension: "jpg|jpeg|gif|png"
            // accept: "jpg,png,jpeg"
            extension: "jpg,jpeg,png", */
            // required: true,
            // accept: "jpg,png,jpeg",

            // filesize: 5, // here we are working with MB
        },
        state: {
            required: true
        },
        city: {
            required: true
        },
        address: {
            required: true
        }
    },
    messages: {
        name: {
            required: "Enter your name",
            minlength: "Please enter your valid name."
        },
        email: {
            required: "Email address is required.",
            email: "Enter valid email address.",
            remote: "Email is already used."
        },
        password: {
            required: "Enter your password.",
            minlength: "Minimum 5 words are required.",
        },
        mobileNumber: {
            required: "Enter your mobile number",
            minlength: "Enter valid mobile number."
        },
        image: {
            /* // extension: "jpg|jpeg|gif|png"
            // accept: "jpg,png,jpeg"
            extension: "jpg,jpeg,png", */
            // required: true,
            // accept: "jpg,png,jpeg",

            // filesize: 5, // here we are working with MB
        },
        state: {
            required: "Enter your state."
        },
        city: {
            required: "Enter your city."
        },
        address: {
            required: "Enter your address."
        }
    }
});

/* $("#pm-register-form").validate({
    // debug : \true,
    rules: {

        first_name: {
            required: true,
            lettersonly: true,
            minlength: 3
        },
        last_name: {
            required: true,
            lettersonly: true,
            minlength: 3
        },
        company: {
            required: true
        },
        others_company: {
            required: true,
            remote: {
                url: "/check-company-name",
                type: "post",
                data: {
                    company_name: function () {
                        return $("#other-company").val();
                    }
                }
            }
        },
        email: {
            required: true,
            email: true,
            remote: {
                url: "/check-email-address",
                type: "post",
                data: {
                    email: function () {
                        return $("#email").val();
                    }
                }
            }
        },
        phone: {
            required: true,
            phoneUS: true,
            minlength: 10
        },
        recaptcha: {
            required: function () {
                if (grecaptcha.getResponse() == '') {
                    return true;
                } else {
                    return false;
                }
            }

        }
    },
    messages: {

        first_name: {
            required: "Enter first name.",
            lettersonly: "Enter valid first name.",
            minlength: "Enter minimum 3 characters."
        },
        last_name: {
            required: "Enter last name.",
            lettersonly: "Enter valid last name.",
            minlength: "Enter minimum 3 characters."
        },
        email: {
            required: "Enter your email address.",
            email: "Enter valid email address.",
            remote: "Email is already used."
        },
        company: {
            required: "Select company name."
        },
        others_company: {
            required: "Enter company name.",
            remote: "Company already exixts."
        },
        phone: {
            phoneUS: "Enter valid phone number",
            required: "Enter phone number"
        }

    }
}) */
