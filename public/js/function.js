$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {

    var stateId = $('#state').val();
    var cityId = $('#city').attr('city');

    $.ajax({
        url: '/get-cities',
        type: "GET",
        data: {
            stateName: stateId
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $('#city').empty();
                $('#city').append('<option value=""> Select City </option>');
                $.map(res.response, function (elementOfArray, indexInArray) {
                    var city_name = elementOfArray['city_name'];
                    if (cityId === city_name) {
                        $('#city').append('<option selected value="' + city_name + '">' + city_name + '</option>');
                    }
                    $('#city').append('<option value="' + city_name + '">' + city_name + '</option>');
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('status => ', xhr.status);
            console.log('error =>', thrownError);
        }
    });
});

/*state select event*/
$('#state').on('change', function () {
    var stateName = $(this).val();
    $.ajax({
        url: '/get-cities',
        type: "GET",
        data: {
            stateName: stateName
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $('#city').empty();
                $('#city').append('<option value=""> Select City </option>');
                $.map(res.response, function (elementOfArray, indexInArray) {
                    var city_name = elementOfArray['city_name'];
                    $('#city').append('<option value="' + city_name + '">' + city_name + '</option>');
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('status => ', xhr.status);
            console.log('error =>', thrownError);
        }
    });
});

/* view user details */
$('.userData').on('click', function (e) {
    e.preventDefault();
    var userid = $(this).data('id');

    $.ajax({
        url: '/users/' + userid,
        type: 'GET',
        data: {
            userid: userid
        },
        success: function (response) {
            const roles = response.roles;

            roles.map(function (res, key) {
                if (parseInt(response.userData.designation) === res.id) {
                    $('#designation').text(res.name);
                }
            });

            $('#name').text(response.userData.name);
            $('#email').text(response.userData.email);
            $('#address').text(response.userData.address);
            $('#mobileNumber').text(response.userData.mobileNo);
            $('#viewState').text(response.userData.state);
            $('#viewCity').text(response.userData.city);
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        },
        error: function (error) {
            console.log('err => ', error);
        }
    });
});

/* delete user data */

$('body').on('click', '.delete-confirm', function (e) {
    // e.preventDefault();
    e.preventDefault(); //cancel default action

    //Recuperate href value
    var href = $(this).attr('href');
    var message = $(this).data('confirm');
    swal({
        title: "Are you sure delete this user ??",
        text: message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: href,
                success: function (data) {
                    swal("Your User data has been deleted!", {
                        icon: "success",
                        timer: 2000,
                        buttons: false,
                        closeOnClickOutside: false
                    });
                    window.setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
            });
        } else {
            swal("Your user data is safe!");
        }
    });
});
