@extends('layouts.app',['title'=>'All Users'])

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                {{-- <div class="card">
                    <div class="card-header">Register</div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="#">

                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">Your Name</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa"
                                                aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="name" id="name"
                                            placeholder="Enter your Name" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">Your Email</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa"
                                                aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="email" id="email"
                                            placeholder="Enter your Email" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="cols-sm-2 control-label">Username</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-users fa"
                                                aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="username" id="username"
                                            placeholder="Enter your Username" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">Password</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password" id="password"
                                            placeholder="Enter your Password" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                                aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="confirm" id="confirm"
                                            placeholder="Confirm your Password" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <button type="button"
                                    class="btn btn-primary btn-lg btn-block login-button">Register</button>
                            </div>
                            <div class="login-register">
                                <a href="index.php">Login</a>
                            </div>
                        </form>
                    </div>

                </div> --}}
                <div class="card">
                    <div class="card-header">
                        <h5 class="mt-2 float-left">{{ __('All Users') }}</h5>
                        <a href="{{ route('users.create') }}" class="btn btn-sm btn-light float-right"
                            title="Add Customer"><i class="fa fa-user-plus fa-2x" aria-hidden="true"></i></a>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead class="table-dark">
                                <tr>
                                    <th>#</th>
                                    <th> {{ __('Name') }}</th>
                                    <th> {{ __('Mobile No') }}</th>
                                    <th> {{ __('Email') }}</th>
                                    <th> {{ __('Profile Image') }}</th>
                                    <th> {{ __('Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (session()->has('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ Session::get('success') }}</strong>
                                    </div>
                                @endif
                                @if (session()->has('error'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ Session::get('error') }}</strong>
                                    </div>
                                @endif
                                @forelse ($data as $key => $value)
                                    <tr>
                                        <th scope="row">{{ $data->firstItem() + $key }}</th>
                                        <td> {{ $value->name }}</td>
                                        <td> {{ $value->email }}</td>
                                        <td> {{ $value->mobileNo }}</td>
                                        <td>
                                            @if ($value->image)
                                                @if (File::exists(public_path('/images/userImage/' . $value->image)))
                                                    <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                                        src="{{ asset('/images/userImage') }}/{{ $value->image }}" />
                                                @else
                                                    <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                                        src="{{ asset('images/blank-img.jpg') }}" />
                                                @endif
                                            @else
                                                <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                                    src="{{ asset('images/blank-img.jpg') }}" />
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-light userData"
                                                data-id="{{ $value->id }}"><i class="fa fa-eye"></i></button>
                                            <a href="{{ route('users.edit', ['user' => $value->id]) }}"
                                                class="btn btn-sm btn-secondary" title="Edit"><i class="fa fa-pencil"
                                                    aria-hidden="true"></i></a>
                                            <a href="{{ route('users.destroy', ['user' => $value->id]) }}"
                                                class="btn btn-sm btn-danger  delete-confirm"><i class="fa fa-trash-o"
                                                    aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-danger text-center" colspan="6">No Users found.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        {!! $data->render() !!}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- view modal --}}
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">View Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body table-responsive">
                    <table class="table text-center table-bordered">
                        <img src="" class="text-center" alt="">
                        <tr>
                            <td><b>Name</b></td>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td id="email"></td>
                        </tr>
                        <tr>
                            <td><b>Designation</b></td>
                            <td id="designation"></td>
                        </tr>
                        <tr>
                            <td><b>Mobile Number</b></td>
                            <td id="mobileNumber"></td>
                        </tr>
                        <tr>
                            <td><b>State</b></td>
                            <td id="viewState"></td>
                        </tr>
                        <tr>
                            <td><b>City</b></td>
                            <td id="viewCity"></td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td id="address"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
