@extends('layouts.app',['title'=>'Edit User Details'])

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit User Details</div>
                    <form class="form-4horizontal" id="editUser" method="post" enctype="multipart/form-data"
                        action="{{ route('users.update', ['user' => $userData->id]) }}">
                        <div class="card-body">
                            @method('PUT')
                            @csrf
                            @if (session()->has('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ Session::get('success') }}</strong>
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ Session::get('error') }}</strong>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">Name<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                        id="name" placeholder="Enter your Name" value="{{ $userData->name }}" />
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">Email Address<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">

                                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                                        name="email" id="email" placeholder="Enter your Email" disabled
                                        value="{{ $userData->email }}" />
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobileNumber" class="cols-sm-2 control-label">Mobile Number<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <input type="text" class="form-control @error('mobileNumber') is-invalid @enderror"
                                        name="mobileNumber" id="mobileNumber" placeholder="Enter your Mobile Number"
                                        value="{{ $userData->mobileNo }}" />
                                    @error('mobileNumber')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="designation" class="cols-sm-2 control-label">Designation</label>
                                <div class="cols-sm-10">
                                    <select name="designation" id="designation" class="form-control">
                                        <option value="">Select Designation</option>
                                        @forelse ($allRoles as $roleKey => $roleValue)
                                            <option value="{{ $roleValue->id }}"
                                                {{ $roleValue->id === (int) $userData->designation ? 'selected = "selected"' : '' }}>
                                                {{ $roleValue->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image" class="cols-sm-2 control-label">Profile Image</label>
                                <div class="cols-sm-10">
                                    @if ($userData->image)
                                        @if (File::exists(public_path('/images/userImage/' . $userData->image)))
                                            <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                                src="{{ asset('/images/userImage') }}/{{ $userData->image }}" />
                                        @else
                                            <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                                src="{{ asset('images/blank-img.jpg') }}" />
                                        @endif
                                    @else
                                        <img height="50" width="70" style="object-fit:fill;border-radius: 10px;"
                                            src="{{ asset('images/blank-img.jpg') }}" />
                                    @endif
                                    <input type="file"
                                        class="form-control @error('image') is-invalid @enderror border border-0"
                                        name="image" id="image" />
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="state1" class="cols-sm-2 control-label">State<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    @php
                                        $state = App\Helpers\StateCityHelper::getStates();
                                    @endphp
                                    <select name="state" id="state"
                                        class="form-control @error('state') is-invalid @enderror">
                                        <option value="">Select State</option>
                                        @foreach ($state as $stateKey => $stateValue)
                                            <option value="{{ $stateValue['state_name'] }}"
                                                {{ $stateValue['state_name'] === $userData->state ? 'selected' : '' }}>
                                                {{ $stateValue['state_name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('state')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="city" class="cols-sm-2 control-label">City<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">

                                    <select name="city" id="city" city={{ $userData->city }}
                                        class="form-control @error('city') is-invalid @enderror">
                                        <option value="">Select City</option>
                                    </select>
                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">Address<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                                        id="address">{{ $userData->address }}</textarea>
                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-sm">{{ __('Update') }}</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
