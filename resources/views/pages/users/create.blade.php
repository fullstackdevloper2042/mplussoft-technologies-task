@extends('layouts.app',['title'=>'Create User'])

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add User</div>
                    <form class="form-4horizontal" id="addUser" method="post" enctype="multipart/form-data"
                        action="{{ route('users.store') }}">
                        <div class="card-body">
                            @csrf
                            @if (session()->has('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ Session::get('success') }}</strong>
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ Session::get('error') }}</strong>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">Name<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                        id="name" placeholder="Enter your Name" />
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">Email Address<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">

                                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                                        name="email" id="email" placeholder="Enter your Email" />
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">Password<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        name="password" id="password" placeholder="Enter your Password" />.
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobileNumber" class="cols-sm-2 control-label">Mobile Number<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <input type="text" class="form-control @error('mobileNumber') is-invalid @enderror"
                                        name="mobileNumber" id="mobileNumber" placeholder="Enter your Mobile Number" />
                                    @error('mobileNumber')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="designation" class="cols-sm-2 control-label">Designation</label>
                                <div class="cols-sm-10">
                                    <select name="designation" id="designation" class="form-control">
                                        <option value="">Select Designation</option>
                                        @forelse ($allRoles as $roleKey => $roleValue)
                                            <option value="{{ $roleValue->id }}">{{ $roleValue->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image" class="cols-sm-2 control-label">Profile Image</label>
                                <div class="cols-sm-10">

                                    <input type="file"
                                        class="form-control @error('image') is-invalid @enderror border border-0"
                                        name="image" id="image" />
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="state" class="cols-sm-2 control-label">State<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    @php
                                        $state = App\Helpers\StateCityHelper::getStates();
                                    @endphp
                                    <select name="state" id="state"
                                        class=" form-control @error('state') is-invalid @enderror">
                                        <option value="">Select State</option>
                                        @foreach ($state as $stateKey => $stateValue)
                                            <option value="{{ $stateValue['state_name'] }}">
                                                {{ $stateValue['state_name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('state')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="city" class="cols-sm-2 control-label">City<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">

                                    <select name="city" id="city" class="form-control @error('city') is-invalid @enderror">
                                        <option value="">Select City</option>
                                    </select>
                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">Address<span
                                        class="text-danger">*</span></label>
                                <div class="cols-sm-10">
                                    <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                                        id="address"></textarea>
                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-sm">{{ __('Add User') }}</button>
                                <button type="reset" class="btn btn-danger btn-sm">{{ __('Reset') }}</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
