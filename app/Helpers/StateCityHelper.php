<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Http;

class StateCityHelper
{
	public static function getStates()
    {
        $generateAuth = new StateCityHelper;
        $token = $generateAuth->generateAuth();
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer '.$token['auth_token'],
                'Accept' => 'application/json'
            ])->get('https://www.universal-tutorial.com/api/states/India');
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        if($response->successful()){
            return $response->json();
        }else{
            return response()->json(['response' =>$response->json(), 'success'=> false], $response->status());
        }
    }

    public function generateAuth(){
        try {
            $response = Http::withHeaders([
                'api-token' =>'xkU7ld11AjvMKka8sNHiLn4KsIFsLXPFA_O8dvzW30HNB89liU-zlbgIFbj1r-L2shw',
                'user-email'=> 'ankit.purohit616@gmail.com',
                'Accept' => 'application/json'
            ])->get('https://www.universal-tutorial.com/api/getaccesstoken');
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        if($response->successful()){
            return $response->json();
        }else{
            return response()->json(['response' =>$response->json(), 'success'=> false], $response->status());
        }
    }
}