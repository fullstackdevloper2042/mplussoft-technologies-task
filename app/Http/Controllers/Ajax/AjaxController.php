<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\User;

class AjaxController extends Controller
{
    public function getAllStates(){
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJhbmtpdC5wdXJvaGl0NjE2QGdtYWlsLmNvbSIsImFwaV90b2tlbiI6InhrVTdsZDExQWp2TUtrYThzTkhpTG40S3NJRnNMWFBGQV9POGR2elczMEhOQjg5bGlVLXpsYmdJRmJqMXItTDJzaHcifSwiZXhwIjoxNjI0NzgwOTUzfQ.0DkctfA4WM5tUsbhUlcgOCbZ6nTv_56nKYDAEekO1nE',
                'Accept' => 'application/json'
            ])->get('https://www.universal-tutorial.com/api/states/India');
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        if($response->successful()){
            return response()->json(['response' =>$response->json(), 'success'=> true], $response->status());
        }else{
            return response()->json(['response' =>$response->json(), 'success'=> false], $response->status());
        }
    }

    /* get cities */
    public function getAllCities(Request $request){
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJhbmtpdC5wdXJvaGl0NjE2QGdtYWlsLmNvbSIsImFwaV90b2tlbiI6InhrVTdsZDExQWp2TUtrYThzTkhpTG40S3NJRnNMWFBGQV9POGR2elczMEhOQjg5bGlVLXpsYmdJRmJqMXItTDJzaHcifSwiZXhwIjoxNjI0NzgwOTUzfQ.0DkctfA4WM5tUsbhUlcgOCbZ6nTv_56nKYDAEekO1nE',
                'Accept' => 'application/json'
            ])->get('https://www.universal-tutorial.com/api/cities/'.$request->stateName);
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        if($response->successful()){
            return response()->json(['response' =>$response->json(), 'success'=> true], $response->status());
        }else{
            return response()->json(['response' =>$response->json(), 'success'=> false], $response->status());
        }
    }

    /* check email address */
    public function checkEmail(Request $request){
        $email = $request->email;
    	$data = User::where('email',$email)->exists();

    	if($data){
            return 'false';
        }else{
            return 'true';
        }
    }
}
