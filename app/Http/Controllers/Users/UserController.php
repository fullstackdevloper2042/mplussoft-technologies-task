<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use Validator;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  User::whereHas('roles', function($role) {
            $role->where('name','User');
        })->paginate(4);
        
        return view('pages.users.list', compact('data'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allRoles = Role::all();
        return view('pages.users.create', compact('allRoles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
        'name'     => 'required |string',
        'email'    => 'required |string | email | max:255 | unique:users',
        'password' => 'required|string|min:5',
        'mobileNumber' => 'required|digits:10',
        'state'    => 'required',
        'city'     => 'required',
        'address'  => 'required',
        'image'    => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ],
        [
            'name.required'       => 'First name is required.',
            'email.required'      => 'Email is required.',
            'email.email'         => 'Please enter a valid email address.',
            'password.required'   => 'Password is required.',
            'password.min'        => 'Minimum 5 character is required.',
            'mobileNumber.required'   => 'Contact number is required.',
            'mobileNumber.digits'     => 'Enter valid mobile number.',
            'state.required'      => 'State is required.',
            'city.required'       => 'City is required.',
            'address.required'    => 'Address is required.',
        ])->validate();
        
        if ($image = $request->file('image')) {
            $destinationPath = 'images/userImage';

            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            // $input['image'] = "$profileImage"; 
            $user = User::create([
                'name'        =>  $request->input('name'),
                'email'       =>  $request->input('email'),
                'password'    =>  Hash::make($request->input('password')),
                'mobileNo'    =>  $request->input('mobileNumber'),
                'designation' =>  $request->input('designation')? $request->input('designation') : 2,
                'state'       =>  $request->input('state'),
                'city'        =>  $request->input('city'),
                'address'     =>  $request->input('address'),
                'image'       => $profileImage
            ]);
            $user->save();
            $user->addRole(Role::User);
        }else{
            $user = User::create([
                'name'        =>  $request->input('name'),
                'email'       =>  $request->input('email'),
                'password'    =>  Hash::make($request->input('password')),
                'mobileNo'    =>  $request->input('mobileNumber'),
                'designation' =>  $request->input('designation')? $request->input('designation') : 2,
                'state'       =>  $request->input('state'),
                'city'        =>  $request->input('city'),
                'address'     =>  $request->input('address'),
            ]);
            $user->save();
            $user->addRole(Role::User);
        }
        if($user){
            $request->session()->flash('success', 'User added successfully .');
            return redirect()->route('users.index');
        }
        $request->session()->flash('error', 'User is not added successfully .');
        return redirect()->route('users.create');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userData = User::findOrfail($id);
        $roles = Role::all();
        if($userData){
            return response()->json(['userData'=>$userData,'roles'=>$roles],200);
        }else{
            return response()->json(['pm_data'=>'Project manager not found.'],403);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userData = User::findOrfail($id);
        $allRoles = Role::all();
        return view('pages.users.edit', compact('allRoles', 'userData'));
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'name'     => 'required |string',
            'mobileNumber' => 'required|digits:10',
            'state'    => 'required',
            'city'     => 'required',
            'address'  => 'required',
            'image'    => 'image|mimes:jpeg,png,jpg,gif|max:2048'
            ],
            [
                'name.required'       => 'First name is required.',
                'mobileNumber.required'   => 'Contact number is required.',
                'mobileNumber.digits'     => 'Enter valid mobile number.',
                'state.required'      => 'State is required.',
                'city.required'       => 'City is required.',
                'address.required'    => 'Address is required.',
            ])->validate();
            
            if ($image = $request->file('image')) {
                $userData = User::findOrfail($id);
                // $oldFilePath = asset('/images/userImage').'/'.$userData->image;
                $oldFilePath = public_path('/images/userImage/' . $userData->image);
                /* check old file and deletefile */
                if (File::exists($oldFilePath)){
                    // unlink($oldFilePath);
                    chmod($oldFilePath, 0644);
                    unlink($oldFilePath);
                    echo 'Deleted old image';
                 }
                $destinationPath = 'images/userImage';
    
                $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                // $input['image'] = "$profileImage"; 
                $user = User::find($id)->update([
                    'name'        =>  $request->input('name'),
                    'mobileNo'    =>  $request->input('mobileNumber'),
                    'designation' =>  $request->input('designation')? $request->input('designation') : 2,
                    'state'       =>  $request->input('state'),
                    'city'        =>  $request->input('city'),
                    'address'     =>  $request->input('address'),
                    'image'       => $profileImage
                ]);
            }else{
                $user = User::find($id)->update([
                    'name'        =>  $request->input('name'),
                    'mobileNo'    =>  $request->input('mobileNumber'),
                    'designation' =>  $request->input('designation')? $request->input('designation') : 2,
                    'state'       =>  $request->input('state'),
                    'city'        =>  $request->input('city'),
                    'address'     =>  $request->input('address'),
                ]);
            }
            if($user){
                $request->session()->flash('success', 'User Profile updated successfully .');
                return redirect()->route('users.index');
            }
            $request->session()->flash('error', 'User profile is not updated.');
            return redirect()->route('users.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $userData = User::find($id)->delete();
        $RoleUser = RoleUser::where('user_id',$id)->delete($id);
        $request->session()->flash('success', 'User Profile deleted successfully');
    }
}
