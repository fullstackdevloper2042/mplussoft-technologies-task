<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    public const Admin = 'Admin';
    public const User = 'User';
    
    public function users(){
        return $this->belongsToMany('App\User')->using('App\Models\RoleUser');
    }
}
