<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Ajax\AjaxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/', function(){
    return view('welcome');
});
Route::get('/t', function () {
    event(new \App\Events\SendMessage());
    // dd('Event Run Successfully.');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function(){
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    /*User Routing*/
    Route::resource('users', UserController::class);

    /* get all states */
    Route::get('all-states', [AjaxController::class, 'getAllStates'])->name('all-states');
    Route::get('get-cities', [AjaxController::class, 'getAllCities'])->name('all-cities');

    /* check email address */
    Route::post('/check-email-address',[AjaxController::class, 'checkEmail']);

    /* Notification mail */
    Route::get('send', [HomeController::class, 'sendNotification']);
});