<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Ankit Purohit',
            'email' => 'ankit.purohit2042@gmail.com',
            'password' => Hash::make('Password@123'),
            'address' => 'Indore',
            'state' => 'MP',
            'city' => 'Indore',
            'mobileNo' => '9522749437',

        ]);
        /* $user->save();
        $user->addRole(Role::Admin); */
        RoleUser::create([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}
